//match height
$('.js-mh').matchHeight();
setTimeout(function () {
	$('.how__product-des, .how__list-des').matchHeight();
}, 0);

//effect
$(window).on('scroll load assessFeatureHeaders', function () {
	var scrollTop = $(window).scrollTop();
	var appearenceBuffer = 60;
	var windowBottom = scrollTop + $(window).height() - appearenceBuffer;
	$('body').toggleClass('scrolled-down', scrollTop > 0);
	$(
		'.js-fadeIn:not(.is-active), .js-fadeUp:not(.is-active), .js-fadeLeft:not(.is-active), .js-fadeRight:not(.is-active), .js-fadeZoom:not(.is-active)',
	)
		.filter(function () {
			var offset = $(this).offset().top;
			var height = $(this).outerHeight();
			return offset + height >= scrollTop && offset <= windowBottom;
		})
		.addClass('is-active');
});

const isMobile =
	/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
		navigator.userAgent,
	);

if (isMobile) {
	$('.leader__list-link').on('click', function (e) {
		e.preventDefault();

		$(this).find('.leader__list-desc').slideToggle();
		$(this).find('.leader__list-str').toggleClass('is-closed');

		if ($(this).find('.leader__list-str').hasClass('is-closed')) {
			$(this).find('.leader__list-mutate').text('Less');
		} else {
			$(this).find('.leader__list-mutate').text('More');
		}
	});
} else {
	$('.leader__list-link').hover(
		function () {
			$(this).find('.leader__list-desc').slideDown();
			$(this).find('.leader__list-str').addClass('is-closed');
			$(this).find('.leader__list-mutate').text('Less');
		},
		function () {
			$(this).find('.leader__list-desc').slideUp();
			$(this).find('.leader__list-str').removeClass('is-closed');
			$(this).find('.leader__list-mutate').text('More');
		},
	);
}
